#!/bin/bash

BINARY="sysops-backend"
GO_PATH="golang"
PROJECT_PATH="sysops-backend"

DOCKER_BUILD_IMAGE="registry.gitlab.com/wjm.elbers/docker-golang-build:0.0.9"

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then #Remote / gitlab ci
        echo "Building ${BINARY} remotely"
        cd ..
        docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${PROJECT_PATH}" -v "$PWD/${GO_PATH}":/go -w "/go/src/${PROJECT_PATH}" "${DOCKER_BUILD_IMAGE}" make
        mv "./${GO_PATH}/src/${PROJECT_PATH}/${BINARY}" ./output
        cp ./output/* ./image
        cd image
    else #Local build
        cd ..
        echo "Building ${BINARY} locally, path=$PWD"
        docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${PROJECT_PATH}" -v "$PWD/${GO_PATH}":/go -w "/go/src/${PROJECT_PATH}" "${DOCKER_BUILD_IMAGE}" make
	      mv "./${GO_PATH}/src/${PROJECT_PATH}/${BINARY}" ./output
        cp ./output/* ./image
        cd ./image
    fi
}

cleanup_data () {
    echo "Removing ${BINARY}"
    rm -f "${BINARY}"
}

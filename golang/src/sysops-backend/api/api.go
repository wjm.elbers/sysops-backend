package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"os"
	"sysops-backend/model"
)

type Api struct {
	data *model.InitialData
	servers map[string]*model.Server
	log *logrus.Logger
	server_filename string
	data_filename string

}

func New(data_dir string, log *logrus.Logger) *Api {
	a := Api{
		data: nil,
		servers: map[string]*model.Server{},
		log: log,
		server_filename: fmt.Sprintf("%s/servers.json", data_dir),
		data_filename: fmt.Sprintf("%s/data.json", data_dir),
	}
	if err := a.loadInitialData(); err != nil {
		log.Infof("Failed to load initial data. Error: %s", err)
	}
	if err := a.loadServers(); err != nil {
		log.Infof("Failed to load server data. Error: %s", err)
	}

	return &a
}

func (a *Api) loadServers() error {
	if _, err := os.Stat(a.server_filename); err != nil {
		if !os.IsNotExist(err) {
			return fmt.Errorf("Cannot stat server data file. Error: %s", err)
		}
	}
	servers_loaded_from_disk := map[string]*model.Server{}
	if err := loadJsonFromDisk(a.server_filename, &servers_loaded_from_disk); err != nil {
		return fmt.Errorf("Failed to load server data from disk. Error: %s")
	}

	a.servers = servers_loaded_from_disk
	a.log.Infof("Loaded server data (%d server) from disk", len(a.servers))
	return nil
}

func (a *Api) loadInitialData() error {
	if _, err := os.Stat(a.data_filename); err != nil {
		if !os.IsNotExist(err) {
			return fmt.Errorf("Cannot stat initial data file. Error: %s", err)
		}
	}

	data_loaded_from_disk := model.InitialData{}
	if err := loadJsonFromDisk(a.data_filename, &data_loaded_from_disk); err != nil {
		return fmt.Errorf("Failed to load initial data from disk. Error: %s")
	}

	a.data = &data_loaded_from_disk
	a.log.Infof("Loaded initial data (%d types, %d hosters) from disk", len(a.data.Types), len(a.data.Hosters))
	return nil
}

func (a *Api) save() {
	json_bytes, err := json.Marshal(a.servers)
	if err != nil {
		a.log.Errorf("Failed to marshal server to JSON: %s", err)
		return
	}

	if err := ioutil.WriteFile(a.server_filename, json_bytes, 0644); err != nil {
		a.log.Errorf("Failed to write server json data to disk: %s", err)
		return
	}
}

func loadJsonFromDisk(filename string, data interface{}) error {
	json_bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return fmt.Errorf("Failed to read server JSON from disk: %s", err)
	}

	if err := json.Unmarshal(json_bytes, data); err != nil {
		return fmt.Errorf("Failed to unmarshal server data from JSON: %s", err)
	}

	return nil
}

func (a *Api) parseRequestBody( r *http.Request, expected_value interface{}) error {
	body_bytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return fmt.Errorf("Failed to read request body: %s", err)
	}

	if err := json.Unmarshal(body_bytes, expected_value); err != nil {
		return fmt.Errorf("Failed to unmarshal request to JSON: %s", err)
	}

	return nil
}

func (a *Api) writeJsonResponse(w http.ResponseWriter, status_code int, data interface{}) {
	json_bytes, err := json.Marshal(data)
	if err != nil {
		a.log.Errorf("Failed to marshal cache to JSON: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status_code)
	w.Write(json_bytes)
}

func (a *Api) GetData(w http.ResponseWriter, r *http.Request) {
	a.writeJsonResponse(w, http.StatusOK, a.data)
}

func (a *Api) GetServers(w http.ResponseWriter, r *http.Request) {
	a.writeJsonResponse(w, http.StatusOK, a.servers)
}

func (a *Api) AddServer(w http.ResponseWriter, r *http.Request) {
	server_import := model.ServerImport{}
	if err := a.parseRequestBody(r, &server_import); err != nil {
		a.log.Errorf("Failed to unmarshal request to JSON: %s", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	key := server_import.Hostname
	if _, ok := a.servers[key]; !ok {
		s, err :=  server_import.ToServer()
		if err != nil {
			a.log.Errorf("Failed to convert submitted server data: %s", err)
		} else {
			a.servers[key] = s
			a.log.Infof("Added server with hostname %s", key)
		}
	} else {
		a.servers[key].Update(server_import)
		a.log.Infof("Updated server with hostname %s", key)
	}

	a.save() //persist changes to disk

	w.WriteHeader(http.StatusCreated)
}

func (a *Api) GetServerDetails(w http.ResponseWriter, r *http.Request) {
	hostname := mux.Vars(r)["hostname"]

	if val, ok := a.servers[hostname]; !ok {
		w.WriteHeader(http.StatusNotFound)
	} else {
		a.writeJsonResponse(w, http.StatusOK, val)
	}
}

func (a *Api) UpdateServerDetails(w http.ResponseWriter, r *http.Request) {
	hostname := mux.Vars(r)["hostname"]
	if val, ok := a.servers[hostname]; !ok {
		w.WriteHeader(http.StatusNotFound)
	} else {
		req := UpdateDetailsRequest{}
		if err := a.parseRequestBody(r, &req); err != nil {
			a.log.Errorf("Failed to unmarshal request to JSON: %s", err)
			w.WriteHeader(http.StatusBadRequest)

		} else {
			val.Type = req.Type
			val.Hoster = req.Hoster

			a.servers[hostname] = val
			a.save()

			w.WriteHeader(http.StatusOK)
		}
	}
}

type UpdateDetailsRequest struct {
	Type *string `json:"type_code"`
	Hoster *string `json:"hoster_code"`
}
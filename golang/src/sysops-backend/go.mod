module sysops-backend

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.8.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
)

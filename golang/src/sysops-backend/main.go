package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"sysops-backend/server"
)

const (
	default_host = "0.0.0.0"
	default_port = 8443
	default_cert_file = "/tls/server.crt"
	default_key_file = "/tls/server.key"
	default_data_dir = "/data"
	default_log_level = "WARN"
	allowed_log_levels = "ERROR, WARN, INFO, DEBUG, TRACE"
)

var log_level string = default_log_level

func main() {
	rootCmd := &cobra.Command{
		Use:   "sysop-backend",
		Short: "Sysops backend server",
		Long: `Sysops backend server CLI`,
	}
	rootCmd.PersistentFlags().StringVar(&log_level, "log", default_log_level, fmt.Sprintf("Set verbosity to the specified level. Allowed values: %s", allowed_log_levels))
	rootCmd.AddCommand(createStartCommand())
	rootCmd.Execute()
}

func getLogger() *logrus.Logger {
	logger := logrus.New()
	logger.Out = os.Stdout
	logger.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})

	switch(log_level) {
	case "TRACE": logger.SetLevel(logrus.TraceLevel)
	case "DEBUG": logger.SetLevel(logrus.DebugLevel)
	case "INFO": logger.SetLevel(logrus.InfoLevel)
	case "WARN": logger.SetLevel(logrus.WarnLevel)
	case "ERROR": logger.SetLevel(logrus.ErrorLevel)
	default:
		logger.Fatalf("Unsupported log level: %s. Allowed values: %s.", log_level, allowed_log_levels)
	}
	logger.Tracef("Setting log level: %v", logger.Level)

	return logger
}

func createStartCommand() *cobra.Command {
	var host string
	var port int
	var cert_file string
	var key_file string
	var data_dir_path string
	var auth_key string

	cmd := &cobra.Command{
		Use:   "start",
		Short: "Start server",
		Long:  `Start server`,
		Run: func(cmd *cobra.Command, args []string) {
			logger := getLogger()
			createDirectory(data_dir_path, logger)
			srv := server.New(host, port, data_dir_path, auth_key, logger)
			if err := srv.StartWithTls(cert_file, key_file); err != nil {
				logger.Fatalf("Failed to start server: %s", err)
			}
		},
	}
	cmd.PersistentFlags().StringVar(&host, "hostname", default_host, "Bind to this ip")
	cmd.PersistentFlags().IntVar(&port, "port", default_port, "Bind to this port")
	cmd.PersistentFlags().StringVar(&cert_file, "cert", default_cert_file, "Certificate file path")
	cmd.PersistentFlags().StringVar(&key_file, "key", default_key_file, "Key file path")
	cmd.PersistentFlags().StringVar(&auth_key, "auth-key", "", "Secret used for authentication")
	cmd.PersistentFlags().StringVar(&data_dir_path, "data-dir", default_data_dir, "Data directory")

	return cmd
}

func createDirectory(dirName string, log *logrus.Logger) bool {
	src, err := os.Stat(dirName)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dirName, 0755)
		if errDir != nil {
			log.Fatalf("Failed to create data dir", err)
		}
		return true
	}

	if src.Mode().IsRegular() {
		log.Fatalf(dirName, "already exist as a file!")
		return false
	}

	return false
}
package model

import (
	"fmt"
	"time"
)

type ServerImport struct {
	UpSince 	*string 	`json:"up_since"`
	Hostname 	 string 	`json:"hostname"`
	IpInfo 		*IpInfo 	`json:"ipinfo"`
	SysInfo 	*SysInfo 	`json:"sysinfo"`
	DeployInfo  *DeployInfo `json:"deploy"`
}

func (s *ServerImport) ToServer() (*Server, error) {
	ts, err := time.Parse("2006-01-02 15:04:05", *s.UpSince)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse up_since timestamp: %s", err)
	}

	server := Server{
		Updates: []time.Time{time.Now()},
		UpSince: ts,
		IpInfo: s.IpInfo,
		SysInfo: s.SysInfo,
		DeployInfo: s.DeployInfo,
	}
	return &server, nil
}

type Type struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

type Hoster struct {
	Code 			 string `json:"code"`
	Name 			 string `json:"name"`
	NameLong		*string `json:"name_long"`
	Type			 string `json:"type"`
	Url				 string `json:"url"`
	Contact 		 string `json:"contact"`
	ControlPanelUrl *string `json:"control_panel_url"`
	StatusUrl 		*string `json:"status_url"`
}

type InitialData struct {
	Types []Type `json:"types"`
	Hosters []Hoster `json:"hosters"`
}

type Server struct {
	Updates		[]time.Time `json:"updates"`
	Hoster		*string		`json:"hoster"`
	Type		*string		`json:"type"`
	UpSince 	time.Time 	`json:"up_since"`
	IpInfo 		*IpInfo 	`json:"ipinfo"`
	SysInfo 	*SysInfo 	`json:"sysinfo"`
	DeployInfo  *DeployInfo `json:"deploy"`
}

func (s *Server) Update(in ServerImport) {
	t := time.Now()
	s.IpInfo = in.IpInfo
	s.SysInfo = in.SysInfo
	s.DeployInfo = in.DeployInfo
	s.Updates = append(s.Updates, t)
}

type IpInfo struct {
	Ip       	string 		`json:"ip"`
	City     	string 		`json:"city"`
	Region   	string 		`json:"region"`
	Country  	string 		`json:"country"`
	Loc      	string 		`json:"loc"`
	Org      	string 		`json:"org"`
	Postal   	string 		`json:"postal"`
	Timezone 	string 		`json:"timezone"`
}

type SysInfo struct {
	Uname 		*string 			`json:"uname"`
	Cpus     	int    				`json:"cpus"`
	CpuModel 	string 				`json:"cpu_model"`
	CpuMhz   	string 				`json:"cpu_mhz"`
	CpuCache 	string 				`json:"cpu_cache"`
	Mem      	string 				`json:"mem"`
	Disks	 	[]SysInfoDisk  		`json:"disks"`
	Partitions 	[]SysInfoPartition	`json:"partitions"`
}

type SysInfoDisk struct {
	Name 		string 			`json:"name"`
	Size 		int64  			`json:"size"`
}

type SysInfoPartition struct {
	Name 		string 			`json:"name"`
	Size 		int64  			`json:"size"`
}

type DeployInfo struct {
	Assets []DeployInfoAssets	 	`json:"assets"`
	Projects []DeployInfoProjects	`json:"projects"`
}

type DeployInfoAssets struct {
	Name string		 `json:"name"`
	Version string	 `json:"version"`
}

type DeployInfoProjects struct {
	Name string		 `json:"name"`
	Remote string	 `json:"remote"`
	Tag string		 `json:"tag"`
	Containers []DeployInfoProjectContainer  `json:"containers"`
}

type DeployInfoProjectContainer struct {
	Id string	 `json:"id"`
	Name string 	 `json:"name"`
	Image string	 `json:"image"`
	Status string	 `json:"status"`
	Ports map[string][]DeployInfoProjectContainerPort  `json:"ports"`
}

type DeployInfoProjectContainerPort struct {
	HostIp string  `json:"HostIp"`
	HostPort string `json:"HostPort"`
}
package server

import (
	"crypto/tls"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
	"sysops-backend/api"
)

type Server struct {
	hostname string
	port int
	log *logrus.Logger
	data_dir_path string
	auth_key string
}

func New(hostname string, port int, data_dir_path, auth_key string, logger *logrus.Logger) *Server {
	if auth_key == "" {
		log.Fatal("Missing auth-key")
	}
	return &Server{
		hostname: hostname,
		port: port,
		log: logger,
		data_dir_path: data_dir_path,
		auth_key: auth_key,
	}
}

func (s *Server) Start() error {
	address := fmt.Sprintf("%s:%d", s.hostname, s.port)
	s.log.Infof("Starting server: %s\n", address)
	srv := &http.Server{
		Addr:         address,
		Handler:      s.getHandler(),
	}
	if err := srv.ListenAndServe(); err != nil {
		return err
	}
	return nil
}

func (s *Server) StartWithTls(cert_file, key_file string) error {
	if _, err := os.Stat(cert_file); os.IsNotExist(err) {
		s.log.Fatalf("Certificate file (%s) not found", cert_file)
	}
	if _, err := os.Stat(key_file); os.IsNotExist(err) {
		s.log.Fatalf("Private key file (%s) not found", key_file)
	}

	cfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}

	address := fmt.Sprintf("%s:%d", s.hostname, s.port)
	s.log.Infof("Starting server with tls: %s\n", address)
	srv := &http.Server{
		Addr:         address,
		Handler:      s.getHandler(),
		TLSConfig:    cfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
	if err := srv.ListenAndServeTLS(cert_file, key_file); err != nil {
		return err
	}
	return nil
}

func (s *Server) getHandler() http.Handler {
	a := api.New(s.data_dir_path, 	s.log)

	router := mux.NewRouter()
	router.Use(s.requestLoggingMiddleware, s.authMiddleware)
	router.HandleFunc("/data", a.GetData).Methods("GET")
	router.HandleFunc("/servers", a.GetServers).Methods("GET")
	router.HandleFunc("/servers", a.AddServer).Methods("POST")
	router.HandleFunc("/servers/{hostname}", a.GetServerDetails).Methods("GET")
	router.HandleFunc("/servers/{hostname}", a.UpdateServerDetails).Methods("POST")

	//Configure CORS
	originsOk := []string{"*"}
	c := cors.New(cors.Options{
		AllowedOrigins: originsOk,
		AllowCredentials: true,
		Debug: false,
	})
	return c.Handler(router)
}

func (s *Server) requestLoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.log.Infof("Request: uri=%s, method=%s", r.RequestURI, r.Method)
		next.ServeHTTP(w, r)
	})
}

func (s *Server) authMiddleware(next http.Handler) http.Handler {
	authZHeaderName := "Authorization"
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		hdr_value := r.Header.Get(authZHeaderName)
		if hdr_value == "" {
			s.log.Errorf("Missing authorization key.")
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		if hdr_value != s.auth_key {
			s.log.Errorf("Invalid authorization key. %s != %s", hdr_value, s.auth_key)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}
